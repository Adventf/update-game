import React from "react";
import { Document, Page, Text, StyleSheet } from "@react-pdf/renderer/lib/react-pdf.browser.cjs.js";

// Create styles
const styles = StyleSheet.create({
  page: {
    backgroundColor: "#E4E4E4",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  title: {
    margin: 25,
    fontSize: 24,
    textAlign: "center",
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "Times-Roman",
  },
});

// Create Document Component
const PdfRenderFile = () => (
  <Document>
    <Page size="A4" style={styles.page}>
      <Text style={styles.title}>BATU GUNTING KERTAS</Text>
      <Text style={styles.text}>
        Batu-Gunting-Kertas adalah sebuah permainan tangan dua orang. Permainan ini sering digunakan untuk pemilihan acak, seperti halnya pelemparan koin, dadu, dan lain-lain. Beberapa permainan dan olahraga menggunakannya untuk menentukan
        peserta mana yang bermain terlebih dahulu. Kadang ia juga dipakai untuk menentukan peran dalam permainan peran, maupun dipakai sebagai sarana perjudian. Permainan ini dimainkan di berbagai belahan dunia. Di kalangan anak-anak
        Indonesia, permainan ini juga dikenal dengan istilah (Suwit Jepang). Di Indonesia dikenal juga permainan sejenis yang dinamakan suwit.
      </Text>
    </Page>
  </Document>
);

export default PdfRenderFile;
