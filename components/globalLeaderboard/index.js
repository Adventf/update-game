import Link from "next/dist/client/link";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { db } from "../../services/firebase-config";
import { collection, getDocs, updateDoc, doc, query, orderBy } from "firebase/firestore";
import styles from "./globalLeaderboard.module.css";

const GlobalLeaderboard = () => {
  const [users, setUsers] = useState([]);
  const usersCollectionRef = collection(db, "users");
  const q = query(usersCollectionRef, orderBy("score", "desc"));

  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(q);
      setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [q]);

  const { currentUser } = useSelector((state) => state.user);

  let rankNow = 0;
  const assignRank = async (id) => {
    const userDoc = doc(db, "users", id);
    const rankFields = { rank: rankNow };
    await updateDoc(userDoc, rankFields);
  };

  return (
    <div className={styles.main}>
      <div className="row">
        <div className="col">
          <h2 className={styles.judul}>GLOBAL LEADERBOARD</h2>
        </div>
      </div>

      <table className={styles.table}>
        <tr className={styles.tr}>
          <th className={styles.th}>Rank</th>
          <th className={styles.th}>Username</th>
          <th className={styles.th}>Total Score</th>
          <Link href="/leaderboard">
            <th className={styles.th}>Game Suit</th>
          </Link>
          <th className={styles.th}>Dummy1</th>
          <th className={styles.th}>Dummy2</th>
        </tr>
        {users.map((user) => {
          rankNow++;
          assignRank(user.id);
          if (user.email == currentUser.email) {
            return (
              <tr className={styles.tr}>
                <td className={styles.td2}>{user.rank}</td>
                <Link href="/profile">
                  <td className={styles.td2}>{user.userName}</td>
                </Link>
                <td className={styles.td2}>{user.score}</td>
                <td className={styles.td2}>{user.scoreSuit}</td>
                <td className={styles.td2}>{user.scoreDummy1}</td>
                <td className={styles.td2}>{user.scoreDummy2}</td>
              </tr>
            );
          } else {
            return (
              <tr className={styles.tr}>
                <td className={styles.td}>{user.rank}</td>
                <Link href={{ pathname: "/seeProfile", query: { data: JSON.stringify(user) } }}>
                  <td className={styles.td}>
                    <a>{user.userName}</a>
                  </td>
                </Link>
                <td className={styles.td}>{user.score}</td>
                <td className={styles.td}>{user.scoreSuit}</td>
                <td className={styles.td}>{user.scoreDummy1}</td>
                <td className={styles.td}>{user.scoreDummy2}</td>
              </tr>
            );
          }
        })}
      </table>

      <br></br>
      <br></br>
    </div>
  );
};

export default GlobalLeaderboard;
