import styles from "../gamesuit/Choice.module.css"
import Image from 'next/image'

export const Choice = ({ value, choiceIcon, onClick }) => {
  return (
    <div value={value} onClick={onClick}>
      <Image className={styles.choiceicon} src={choiceIcon} alt="icon" />
    </div>
  );
};
