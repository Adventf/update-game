import React, { useState, useEffect, useContext } from "react";
import { db } from '../../services/firebase-config';
import { collection, getDocs, updateDoc, doc } from "firebase/firestore";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";


const UpdateScore = () => {
  
  const loginUsername = useContext(UserContext)
  const [curUser, setCurUser] = useState({})
  const [curEmail, setCurEmail] = useState("")

  const usersCollectionRef = collection(db, "users")


    const updateUser = async (id) => {
        const userDoc = doc(db, "users", id)
        if(curEmail!==""){
          const newEmail = { email: curEmail }
          await updateDoc(userDoc, newEmail)
        }
        else {
          alert("Invalid entry")
        }
    }

    useEffect(() => {
      const getUsers= async () => {
          const data = await getDocs(usersCollectionRef)
          let allUsers = data.docs.map((doc) => ({...doc.data(), id: doc.id}));
          setCurUser(allUsers.find(x => x.username == loginUsername))
      }
      getUsers()
    }, [loginUsername, usersCollectionRef])
    
    const handleChange = (e) => {
      e.preventDefault();
      let target = e.target;
      let value = target.value;

      setCurEmail(value)
      console.log(curEmail);
  };


    return (
      <div className="container">
        <form className="card">
          <h1 className="title">Username: {curUser.username}</h1>
          <div className="text mt-5 mb-5">
            <label>Email:</label>
            <input 
              type="text" 
              name="email" 
              placeholder={curUser.email} 
              value={curEmail}
              onChange={(e) => handleChange(e)}
            />
          </div>

          <div className="btnContainer">
            <Link to="/profile">
              <button onClick={()=>{updateUser(curUser.id)}}>Submit</button>
            </Link>
          </div>
        </form>
      </div>
    );
  };
  export default UpdateScore;
  