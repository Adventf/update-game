import { settings } from "./configs/game";
import styles from "../gamesuit/Reset.module.css";

export const Reset = ({ onClick, userSelection, userScore, pcScore }) => {
  return (
    userSelection !== "" && (
      <div onClick={onClick} className={styles.resetbtn}>
        <h3>{userScore === settings.winTarget || pcScore === settings.winTarget ? "Play again" : "Reset"}</h3>
      </div>
    )
  );
};
