import { settings } from "./configs/game";
import styles from "../gamesuit/User.module.css";
import Image from 'next/image'

export const User = ({ userScore, userSelection, trophyIcon, children }) => {
  return (
    <div className={styles.usercard}>
      <h1>{settings.userName}</h1>
      {userScore < settings.winTarget ? (
        <>
          <div className={styles.choicegrid}>{children}</div>
          <h3>{userSelection === "" ? "Pick one!" : `Your choice: ${userSelection}`}</h3>
        </>
      ) : (
        <>
          <Image src={trophyIcon} alt="trophy" />
          <h3>Victory!</h3>
        </>
      )}
    </div>
  );
};
