import styles from "../gamesuit/Playground.module.css";

export const Playground = ({ children }) => {
  return <div className={styles.playarea}>{children}</div>;
};
