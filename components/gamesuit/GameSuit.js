import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Title } from "./Title";
import { Round } from "./Round";
import { Playground } from "./Playground";
import { Profile } from "./Profile";
import { User } from "./User";
import { Choice } from "./Choice";
import { Computer } from "./Computer";
import { Score } from "./Score";
import { Message } from "./Message";
import { Reset } from "./Reset";
import { db } from "../../services/firebase-config";
import { updateDoc, getDocs, collection, doc } from "firebase/firestore";
import { useSelector } from "react-redux";

import { settings } from "./configs/game";

import rock from "../gamesuit/assets/batu.png";
import paper from "../gamesuit/assets/kertas.png";
import scissors from "../gamesuit/assets/gunting.png";
import trophy from "../gamesuit/assets/trophy.png";

import styles from "../gamesuit/styles.module.css";

export default function GamePages() {
  let [game, setGame] = useState({
    userSelection: "",
    pcSelection: "",
    round: 0,
    userScore: 0,
    pcScore: 0,
    message: "",
  });

  const reset = () => {
    setGame({
      ...game,
      userSelection: "",
      pcSelection: "",
      round: 0,
      userScore: 0,
      pcScore: 0,
      message: "",
    });
  };

  const { winMessage, tieMessage, lostMessage, winTarget } = settings;
  const { pcScore, userScore } = game;

  const play = (e) => {
    if (pcScore < winTarget) {
      const userSelection = e.target.parentNode.parentNode.getAttribute("value");
      console.log("user throws: " + userSelection);
      const pcSelection = ["Rock", "Paper", "Scissors"][Math.floor(Math.random() * 3)];
      console.log("PC throws: " + pcSelection);
      userSelection === pcSelection
        ? setGame({
            ...(game.message = tieMessage),
          })
        : (userSelection === "Rock" && pcSelection === "Scissors") || (userSelection === "Paper" && pcSelection === "Rock") || (userSelection === "Scissors" && pcSelection === "Paper")
        ? setGame({
            ...(game.userScore += 1),
            ...(game.message = winMessage),
          })
        : setGame({
            ...(game.pcScore += 1),
            ...(game.message = lostMessage),
          });

      setGame({
        ...game,
        round: (game.round += 1),
        userSelection,
        pcSelection,
      });
    }
  };
  const usersCollectionRef = collection(db, "users");
  const [curUser, setCurUser] = useState({});
  const { currentUser } = useSelector((state) => state.user);
  // const dispatch = useDispatch();
  useEffect(() => {
    const getUser = async () => {
      const data = await getDocs(usersCollectionRef);
      let allUsers = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
      setCurUser(allUsers.find((x) => x.email == currentUser.email));
    };
    getUser();
  }, [usersCollectionRef, currentUser]);

  const addScore = async (x) => {
    console.log("adding score " + x);
    const userDoc = doc(db, "users", curUser.id);
    const newFields = {
      score: curUser.score + x,
      scoreSuit: curUser.scoreSuit + x,
    };
    await updateDoc(userDoc, newFields);
  };

  if (game.userScore == winTarget) {
    console.log("GAME WIN!!!");
    addScore(30);
  }

  if (game.pcScore == winTarget) {
    console.log("GAME LOSE!!!");
    addScore(1);
  }

  return (
    <div id={styles.suit} className={styles.GameDetailPages}>
      <div className={styles.divright}>
        <Link href="/">
          <a>
            <button className={styles.btngame}>close</button>
          </a>
        </Link>
      </div>

      <Title />
      <Round {...game} />
      <Playground>
        <Profile>
          <User {...game} trophyIcon={trophy}>
            <Choice {...game} value="Rock" onClick={play} choiceIcon={rock} />
            <Choice {...game} value="Paper" onClick={play} choiceIcon={paper} />
            <Choice {...game} value="Scissors" onClick={play} choiceIcon={scissors} />
          </User>
          <Score score={userScore} />
        </Profile>
        <Message {...game} />
        <Profile>
          <Computer {...game} rockIcon={rock} paperIcon={paper} scissorsIcon={scissors} trophyIcon={trophy} />
          <Score score={pcScore} />
        </Profile>
      </Playground>
      <Reset {...game} onClick={reset} />
    </div>
  );
}

