import { React } from "react";
import { Container, Stack } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import styles from "../gamesuit/index.module.css";

export default function StickyHeadTable() {
  return (
    <Container>
      <Stack>
        <h2 className={styles.leaderboard}>Leaderboard</h2>
      </Stack>
      <div id={styles.tablewrapper}>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Username</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Fabio</td>
              <td>Rosso</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </Table>
      </div>
    </Container>
  );
}
