import styles from "../gamesuit/Round.module.css";

export const Round = ({ userSelection, round }) => {
  return (
    <h1 className={styles.round}>
      {userSelection === "" ? "No rounds yet!" : `Round: ${round}`}
    </h1>
  );
};
