import React from 'react';
import styles from './footer.module.css';
import Image from 'next/image'

const Footer = () => {
	return (
		<div>
			<div className={styles.orange_line}></div>
		<div className={styles.footer}>
			
			<h1 className={styles.footer}>&copy;Kelompok 2 - FSW WAVE 16 - Binar Academy</h1>
			{/* <img className={styles.img} src="./facebook2.png" alt=""/>
			<img className={styles.img} src="./youtube.png" alt=""/>
			<img className={styles.img2} src="./twitter.png" alt=""/> */}
		<Image src="/facebook2.png" width={30} height={30} alt="fb" />
		<Image src="/youtube.png" width={30} height={30} alt="bf" />
		<Image src="/twitter.png" width={30} height={30} alt="fbb" />
		</div>
		</div>
	);
};

export default Footer;