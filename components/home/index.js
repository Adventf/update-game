import React, { useState, useEffect } from "react";
import { db } from "../../services/firebase-config";
import { collection, getDocs, updateDoc, doc } from "firebase/firestore";
import { CardGroup, Card, CardImg, Button, CardTitle, PopoverHeader, PopoverBody, UncontrolledPopover } from "reactstrap";
import { useSelector } from "react-redux";
import Link from "next/link";
import styles from "./home.module.css";

const Home = () => {
  let isPlayed = false;
  let isPlayed2 = false;
  let isPlayed3 = false;
  const [curUser, setCurUser] = useState({});
  const usersCollectionRef = collection(db, "users");
  const { currentUser } = useSelector((state) => state.user);

  useEffect(() => {
    const getUser = async () => {
      const data = await getDocs(usersCollectionRef);
      let allUsers = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
      setCurUser(allUsers.find((x) => x.email == currentUser?.email));
      console.log(curUser);
    };
    getUser();
  }, [usersCollectionRef, currentUser, curUser]);

  if (currentUser) {
    if (curUser.scoreSuit > 0) isPlayed = true;
    console.log(curUser.score + "   suit    " + isPlayed);

    if (curUser.scoreDummy1 > 0) isPlayed2 = true;
    console.log(curUser.score + "   dummy1    " + isPlayed2);

    if (curUser.scoreDummy2 > 0) isPlayed3 = true;
    console.log(curUser.scoreDummy2 + "   dummy2    " + isPlayed3);
  }

  const [randScore, setRandScore] = useState(0);

  const randomScore = async () => {
    const ValorMax = 100;
    const ValorMin = 0;
    const newNumber = parseInt(Math.random() * (ValorMax - ValorMin)) + ValorMin;
    setRandScore(newNumber);
    if (ValorMax < ValorMin) {
      alert("Max value should be higher than Min value");
      setRandScore("Error");
    }
    const id = curUser.id;
    const userDoc = doc(db, "users", id);

    if (currentUser) {
      const newFields = {
        score: curUser.score + newNumber,
        scoreDummy1: curUser.scoreDummy1 + newNumber,
      };
      await updateDoc(userDoc, newFields);
    }
  };

  const randomScore2 = async () => {
    const ValorMax = 100;
    const ValorMin = 0;
    const newNumber = parseInt(Math.random() * (ValorMax - ValorMin)) + ValorMin;
    setRandScore(newNumber);
    if (ValorMax < ValorMin) {
      alert("Max value should be higher than Min value");
      setRandScore("Error");
    }
    const id = curUser.id;
    const userDoc = doc(db, "users", id);

    if (currentUser) {
      const newFields = {
        score: curUser.score + newNumber,
        scoreDummy2: curUser.scoreDummy2 + newNumber,
      };
      await updateDoc(userDoc, newFields);
    }
  };

  const img1 = "./assets/Crazy-Roll-3D.jpg";
  const img2 = "./assets/Bullet-Force.jpg";
  const img3 = "./assets/Highway-Racer.png";
  const img4 = "./assets/air-wars-3.jpg";
  const img5 = "./assets/bounce-blocku-golf.jpg";
  const img6 = "./assets/bullet-bonanza.png";
  const img7 = "./assets/flyordieio.jpg";
  const img8 = "./assets/rockpaperstrategy-1600.jpg";
  const img9 = "./assets/getting-over-it.jpg";
  const img10 = "./assets/infiltrating-the-airship.jpg";
  const img11 = "./assets/rocket-bot-royale.jpg";
  const img12 = "./assets/soccer-legends-2021.jpg";
  const img13 = "./assets/stabfish-io.jpg";
  const img14 = "./assets/the-mage.jpg";

  return (
    <div className="home-body">
      <CardGroup>
        <Card className={styles.category}>
          <CardTitle>
            <h1>COMING SOON</h1>
          </CardTitle>
        </Card>
      </CardGroup>

      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img1} top width="100%" />
          <Button className={styles.button1}>Crazy Roll 3D</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img2} top width="100%" />
          <Button className={styles.button2}>Bullet Force</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img3} top width="100%" />
          <Button className={styles.button1}>Highway Racer</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img4} top width="100%" />
          <Button className={styles.button2}>Air Wars 3</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img5} top width="100%" />
          <Button className={styles.button1}>Bounce Blocku Golf</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img6} top width="100%" />
          <Button className={styles.button2}>Bullet Bonanza</Button>
        </Card>
      </CardGroup>

      <CardGroup>
        <Card className={styles.category}>
          <CardTitle>
            <h1>TOP GAMES</h1>
          </CardTitle>
        </Card>
      </CardGroup>

      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img7} top width="100%" />
          {isPlayed2 ? (
            <Button className={styles.button1} id="Popover1" type="button" onClick={randomScore}>
              Fly Ordie.io (Played Before)
            </Button>
          ) : (
            <Button className={styles.button1} id="Popover1" type="button" onClick={randomScore}>
              Fly Ordie.io
            </Button>
          )}
          <UncontrolledPopover trigger="click" placement="bottom" target="Popover1">
            <PopoverHeader> Your Score </PopoverHeader>
            <PopoverBody>{randScore}</PopoverBody>
          </UncontrolledPopover>
        </Card>
        <Card>
          <h1> </h1>
          <h1> </h1>
          <h1> </h1>
          <CardImg alt="Card image cap" src={img8} top width="100%" />
          {isPlayed ? (
            <Link href="/detail">
              <a>
                <Button className={styles.button3}>Rock-Paper-Scissors (Played Before)</Button>
              </a>
            </Link>
          ) : (
            <Link href="/detail">
              <a>
                <Button className={styles.button3}>Rock-Paper-Scissors</Button>
              </a>
            </Link>
          )}
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img9} top width="100%" />
          {isPlayed3 ? (
            <Button className={styles.button1} id="Popover3" type="button" onClick={randomScore2}>
              Getting Over It (Played Before)
            </Button>
          ) : (
            <Button className={styles.button1} id="Popover3" type="button" onClick={randomScore2}>
              Getting Over It
            </Button>
          )}
          <UncontrolledPopover trigger="click" placement="bottom" target="Popover3">
            <PopoverHeader> Your Score </PopoverHeader>
            <PopoverBody>{randScore}</PopoverBody>
          </UncontrolledPopover>
        </Card>
      </CardGroup>

      <CardGroup>
        <Card className={styles.category}>
          <CardTitle>
            <h1>GAME LIST</h1>
          </CardTitle>
        </Card>
      </CardGroup>

      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img10} top width="100%" />
          <Button className={styles.button1}>Infiltrating The Airship</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img11} top width="100%" />
          <Button className={styles.button2}>Rocket Bot Royale</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img12} top width="100%" />
          <Button className={styles.button1}>Soccer Legends 2021</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img13} top width="100%" />
          <Button className={styles.button2}>Stabfish.io</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img14} top width="100%" />
          <Button className={styles.button1}>The Mage</Button>
        </Card>
      </CardGroup>
      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img1} top width="100%" />
          <Button className={styles.button2}>Crazy Roll 3D</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img2} top width="100%" />
          <Button className={styles.button1}>Bullet Force</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img3} top width="100%" />
          <Button className={styles.button2}>Highway Racer</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img4} top width="100%" />
          <Button className={styles.button1}>Air Wars 3</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img5} top width="100%" />
          <Button className={styles.button2}>Bounce Blocku Golf</Button>
        </Card>
      </CardGroup>
      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img10} top width="100%" />
          <Button className={styles.button1}>Infiltrating The Airship</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img11} top width="100%" />
          <Button className={styles.button2}>Rocket Bot Royale</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img12} top width="100%" />
          <Button className={styles.button1}>Soccer Legends 2021</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img13} top width="100%" />
          <Button className={styles.button2}>Stabfish.io</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img14} top width="100%" />
          <Button className={styles.button1}>The Mage</Button>
        </Card>
      </CardGroup>
      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img1} top width="100%" />
          <Button className={styles.button2}>Crazy Roll 3D</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img2} top width="100%" />
          <Button className={styles.button1}>Bullet Force</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img3} top width="100%" />
          <Button className={styles.button2}>Highway Racer</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img4} top width="100%" />
          <Button className={styles.button1}>Air Wars 3</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img5} top width="100%" />
          <Button className={styles.button2}>Bounce Blocku Golf</Button>
        </Card>
      </CardGroup>
      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img10} top width="100%" />
          <Button className={styles.button1}>Infiltrating The Airship</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img11} top width="100%" />
          <Button className={styles.button2}>Rocket Bot Royale</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img12} top width="100%" />
          <Button className={styles.button1}>Soccer Legends 2021</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img13} top width="100%" />
          <Button className={styles.button2}>Stabfish.io</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img14} top width="100%" />
          <Button className={styles.button1}>The Mage</Button>
        </Card>
      </CardGroup>
      <CardGroup>
        <Card>
          <CardImg alt="Card image cap" src={img1} top width="100%" />
          <Button className={styles.button2}>Crazy Roll 3D</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img2} top width="100%" />
          <Button className={styles.button1}>Bullet Force</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img3} top width="100%" />
          <Button className={styles.button2}>Highway Racer</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img4} top width="100%" />
          <Button className={styles.button1}>Air Wars 3</Button>
        </Card>
        <Card>
          <CardImg alt="Card image cap" src={img5} top width="100%" />
          <Button className={styles.button2}>Bounce Blocku Golf</Button>
        </Card>
      </CardGroup>
    </div>
  );
};
export default Home;
