import styles from "../detailgame/detail.module.css";
import Link from "next/link";
import Image from "next/image";

const Detail = () => {
  return (
    <div className={styles.container}>
      <div className={styles.column}>
        <Image src="/assets/rockpaperstrategy-1600.jpg" width={700} height={400} alt="gambar" />
      </div>
      <div className={styles.column}>
        <h2 className={styles.judul}>BATU GUNTING KERTAS</h2>
        <hr />
        <p>
          Batu-Gunting-Kertas adalah sebuah permainan tangan dua orang. Permainan ini sering digunakan untuk pemilihan acak, seperti halnya pelemparan koin, dadu, dan lain-lain. Beberapa permainan dan olahraga menggunakannya untuk
          menentukan peserta mana yang bermain terlebih dahulu. Kadang ia juga dipakai untuk menentukan peran dalam permainan peran, maupun dipakai sebagai sarana perjudian. Permainan ini dimainkan di berbagai belahan dunia. Di kalangan
          anak-anak Indonesia, permainan ini juga dikenal dengan istilah (Suwit Jepang). Di Indonesia dikenal juga permainan sejenis yang dinamakan suwit.
        </p>
        <Link href="/pdfgenerator">
          <a>
            <button className={styles.btndetail}>Print PDF</button>
          </a>
        </Link>
        <Link className={styles.btndetail} href="/videos">
          <a>
            <button className={styles.btndetail}>Upload Video</button>
          </a>
        </Link>
        <Link href="/gamesuit">
          <a>
            <button className={styles.btndetail}>PLAY GAME</button>
          </a>
        </Link>
      </div>
    </div>
  );
};

export default Detail;
