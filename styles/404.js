import Image from "next/image"
import Link from "next/link"
import styles from '../styles/Error.module.css'

export default function Custom404() {
   
  return (
    <div>
      <Image className={styles.landingImage}
      src="/404.png"
      alt="Picture"
      layout="fill"
      objectFit="cover"
      objectPosition="center"
      />
      <div className={styles.goHome}>
        <Link href='/'>
          <a>
            <button 
              className={styles.backHome} 
              type="button"
              id="btn-signup"
              >
              <i className='fas fa-user-plus'></i>
                Back Home
            </button>
          </a>
        </Link>
      <h3 className={styles.landingText}>Are you lost?</h3>
    
    </div>
    </div>
  )
}
