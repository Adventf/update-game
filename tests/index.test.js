import Custom404 from "../pages/404";
import Videos from "../pages/videos"
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

describe("404 Page", () => {
    it("renders 404 Not Found", () => {
      render(<Custom404 />);
      // check if all components are rendered
      const heading404 = screen.getByText(
        /Are you lost/i
      );

      expect(screen.getByTestId("go-home")).toBeInTheDocument();
      expect(heading404).toBeInTheDocument();

    });
    it("auto back home", () => {
      render(<Custom404 />);

      expect(screen.getByTestId("welcome")).toBeInTheDocument();
    })
  });

  describe("video page", () => {
    it("renders video input", () => {
      render(<Videos />);
      const headingVideo = screen.getByText(
        /Video upload/i
      );

      expect(screen.getByTestId("video-input")).toBeInTheDocument();
      expect(headingVideo).toBeInTheDocument();
    })
    it("renders image input", () => {
      render(<Videos />);

      expect(screen.getByTestId("image-input")).toBeInTheDocument();
    })
    it("renders pdf input", () => {
      render(<Videos />);

      expect(screen.getByTestId("pdf-input")).toBeInTheDocument();
    })
    
    
  })