import * as types from "./actionTypes"
import { auth, googleAuthProvider, facebookAuthProvider, githubAuthProvider } from '../services/firebase-config.js'


const registerStart = () => ({
    type: types.REGISTER_START,
})

const registerSuccess = (user) => ({
    type: types.REGISTER_SUCCESS,
    payload: user,
})

const registerFail = (error) => ({
    type: types.REGISTER_FAIL,
    payload: error,
})

const loginStart = () => ({
    type: types.LOGIN_START,
})

const loginSuccess = (user) => ({
    type: types.LOGIN_SUCCESS,
    payload: user,
})

const loginFail = (error) => ({
    type: types.LOGIN_FAIL,
    payload: error,
})

const googleSignInStart = (user) => ({
    type: types.GOOGLE_SIGN_IN_START,
    payload: user,
})
const googleSignInSuccess = () => ({
    type: types.GOOGLE_SIGN_IN_SUCCESS,
})

const googleSignInFail = (error) => ({
    type: types.GOOGLE_SIGN_IN_FAIL,
    payload: error,
})

const facebookSignInStart = (user) => ({
    type: types.FACEBOOK_SIGN_IN_START,
    payload: user,
})
const facebookSignInSuccess = () => ({
    type: types.FACEBOOK_SIGN_IN_SUCCESS,
})

const facebookSignInFail = (error) => ({
    type: types.FACEBOOK_SIGN_IN_FAIL,
    payload: error,
})

const githubSignInStart = (user) => ({
    type: types.GITHUB_SIGN_IN_START,
    payload: user,
})
const githubSignInSuccess = () => ({
    type: types.GITHUB_SIGN_IN_SUCCESS,
})

const githubSignInFail = (error) => ({
    type: types.GITHUB_SIGN_IN_FAIL,
    payload: error,
})

const logoutStart = () => ({
    type: types.LOGOUT_START,
})

const logoutSuccess = () => ({
    type: types.LOGOUT_SUCCESS,
})

const logoutFail = (error) => ({
    type: types.LOGOUT_FAIL,
    payload: error,
})

export const registerInitiate = (email, password, userName, rank, scoreSuit, scoreDummy1, scoreDummy2 ) => {
    return function (dispatch) {
        dispatch(registerStart());
        auth
        .createUserWithEmailAndPassword(email, password)
        .then(({ user }) => {
            user.updateProfile({
                userName,
                rank,
                scoreSuit,
                scoreDummy1,
                scoreDummy2
            })
            dispatch(registerSuccess(user));
            window.location.href = "/Auth/Login"
        })
        .catch((error) => {dispatch(registerFail(error.message))
        alert(error.message)});
    }
}

export const loginInitiate = (email, password) => {
    return function (dispatch) {
        dispatch(loginStart());
        auth
        .signInWithEmailAndPassword(email, password)
        .then(({ user }) => {
            dispatch(loginSuccess(user));
            // window.location.href = "/"
        })
        .catch((error) => {dispatch(loginFail(error.message))
        alert(error.message)});
    }
}

export const googleSignInInitiate = () => {
    return function (dispatch) {
        dispatch(googleSignInStart());
        auth
        .signInWithPopup(googleAuthProvider)
        .then(({ user }) => {
            dispatch(googleSignInSuccess(user));
            window.location.href = "/"
        })
        .catch((error) => dispatch(googleSignInFail(error.message)));
    }
} 

export const facebookSignInInitiate = () => {
    return function (dispatch) {
        dispatch(facebookSignInStart());
        auth
        .signInWithPopup(facebookAuthProvider.addScope("user_birthday, email"))
        .then(({ user }) => {
            dispatch(facebookSignInSuccess(user));
            window.location.href = "/"
        })
        .catch((error) => dispatch(facebookSignInFail(error.message)));
    }
}

export const githubSignInInitiate = () => {
    return function (dispatch) {
        dispatch(githubSignInStart());
        auth
        .signInWithPopup(githubAuthProvider)
        .then(({ user }) => {
            dispatch(githubSignInSuccess(user));
            window.location.href = "/"
        })
        .catch((error) => dispatch(githubSignInFail(error.message)));
    }
}

export const logoutInitiate = () => {
    return function (dispatch) {
        dispatch(logoutStart());
        auth
        .signOut()
        .then ((resp) => {dispatch(logoutSuccess())
        //  window.location.href = "/"
        })
        .catch((error) => dispatch(logoutFail(error.message)));
    }
}
