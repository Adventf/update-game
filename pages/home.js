import Index from '../components/home/index';
import styles from '../styles/Home.module.css';

function Home() {
	return (
		<div className={styles.main}>
			<Index />
		</div>
	);
}

export default Home;
