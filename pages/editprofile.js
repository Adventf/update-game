import { useState, useEffect } from "react";
import Link from "next/link";
import { db } from "../services/firebase-config";
import { collection, getDocs, updateDoc, doc } from "firebase/firestore";
import styles from "../styles/Profile.module.css";
import { useSelector } from "react-redux";

const Editprofile = () => {
  const [curUser, setCurUser] = useState({});
  const [curUserName, setCurUserName] = useState("");
  const [curPassword, setCurPassword] = useState("");

  const usersCollectionRef = collection(db, "users");
  const { currentUser } = useSelector((state) => state.user);

  const updateUser = async (id) => {
    const userDoc = doc(db, "users", id);
    if (curUserName !== "") {
      const newUserName = { userName: curUserName };
      await updateDoc(userDoc, newUserName);
    }
    if (curPassword !== "") {
      const newPassword = { password: curPassword };
      await updateDoc(userDoc, newPassword);
    } else {
      alert("Invalid entry");
    }
  };

  useEffect(() => {
    const getUser = async () => {
      const data = await getDocs(usersCollectionRef);
      let allUsers = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
      setCurUser(allUsers.find((x) => x.email == currentUser?.email));
    };
    getUser();
  }, [usersCollectionRef, currentUser]);

  return (
    <div className={styles.main}>
      <form className={styles.profileCard}>
        <h1 className={styles.profileTitle}>Email: {curUser.email}</h1>
        <div className={[styles.profileText, styles.mt].join(" ")}>
          <label>Username: </label>
          <input
            type="text"
            name="userName"
            placeholder={curUser.userName}
            value={curUserName}
            // onChange={(e) => handleChange(e)}
            onChange={(e) => {
              setCurUserName(e.target.value);
            }}
          />
        </div>
        <br></br>
        <div className={[styles.profileText, styles.mb].join(" ")}>
          <label>Password:&nbsp;</label>
          <input
            type="password"
            name="password"
            placeholder="*****"
            value={curPassword}
            // onChange={(e) => handleChange(e)}
            onChange={(e) => {
              setCurPassword(e.target.value);
            }}
          />
        </div>
        <div className={styles.btnContainer}>
          <Link href="/profile">
            <a>
              <button
                className={styles.profileButton}
                onClick={() => {
                  updateUser(curUser.id);
                }}
              >
                Submit
              </button>
            </a>
          </Link>
        </div>
      </form>
    </div>
  );
};
export default Editprofile;
