import Index from "../components/detailgame/index";
import { useSelector } from "react-redux";
import Link from "next/link";
import styles from "../styles/Home.module.css";

function Detail() {
  const { currentUser } = useSelector((state) => state.user);

  if (currentUser) {
    return (
      <div className={styles.main}>
        <Index />
      </div>
    );
  } else {
    return (
      <div className={styles.main}>
        <h2 className={styles.username}>You must log in first!</h2>
        <Link href="/Auth/Login">
          <a>
            <h3 className={styles.card}>LOG IN</h3>
          </a>
        </Link>
      </div>
    );
  }
}

export default Detail;
