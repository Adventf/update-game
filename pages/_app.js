import '../styles/globals.css'
import {Provider} from 'react-redux';
import {store} from '../redux/store';
import NavBar from '../components/navbar'
import Footer from '../components/footer';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
 return (
    <Provider store={store}>
      <NavBar/>
      <Component {...pageProps} />
      <Footer/>
    </Provider>
  )
}

export default MyApp
