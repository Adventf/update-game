import React from "react";
import VideoInput from "../components/videos/videos";

export default function App() {
  return (
    <div className="App" data-testid="video-input">
      <h1>Video Upload</h1>
      <VideoInput width={400} height={300} />
    </div>
  );
}
