import { useState, useEffect } from "react";
import Link from "next/link";
import { db } from "../services/firebase-config";
import { collection, getDocs } from "firebase/firestore";
import styles from "../styles/Profile.module.css";
import { useSelector } from "react-redux";

const Profile = () => {
  const [curUser, setCurUser] = useState({});
  const usersCollectionRef = collection(db, "users");
  const { currentUser } = useSelector((state) => state.user);

  useEffect(() => {
    const getUser = async () => {
      const data = await getDocs(usersCollectionRef);
      let allUsers = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
      setCurUser(allUsers.find((x) => x.email == currentUser?.email));
    };
    getUser();
  }, [usersCollectionRef, currentUser]);

  if (currentUser) {
    return (
      <div className={styles.main}>
        <form className={styles.profileCard}>
          <h1 className={styles.profileTitle}>Username: {curUser.userName}</h1>
          <p className={[styles.profileText, styles.mt].join(" ")}>Email: {curUser.email}</p>
          <p className={[styles.profileText, styles.mb].join(" ")}>Rank: {curUser.rank}</p>
          <p className={styles.profileText}>Total Score: {curUser.score}</p>
          <p className={styles.profileText}>Suit Score: {curUser.scoreSuit}</p>
          <p className={styles.profileText}>Dummy1 Score: {curUser.scoreDummy1}</p>
          <p className={styles.profileText}>Dummy2 Score: {curUser.scoreDummy2}</p>
          <div className={styles.btnContainer}>
            <Link href="/editprofile">
              <a>
                <button className={styles.profileButton}>Edit</button>
              </a>
            </Link>
            <Link href="/globalLeaderboard">
              <a>
                <button className={styles.profileButton2}>All Players</button>
              </a>
            </Link>
          </div>
        </form>
        {/* </div> */}
      </div>
    );
  } else {
    return (
      <div className={styles.main}>
        <h2 className={styles.profileText}>Log in first!</h2>
      </div>
    );
  }
};
export default Profile;
