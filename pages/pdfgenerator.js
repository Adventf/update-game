import PdfRenderFile from "../components/pdffilerender/pdfrend";
import { PDFViewer } from "@react-pdf/renderer/lib/react-pdf.browser.cjs.js";

function PdfGenerator() {
  return (
    <PDFViewer style={{ width: "100%", height: "100vh" }}>
      <PdfRenderFile />
    </PDFViewer>
  );
}

export default PdfGenerator;
