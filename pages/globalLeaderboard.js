import Index from "../components/globalLeaderboard/index";
import { useSelector } from "react-redux";
import Link from "next/link";
import styles from "../components/globalLeaderboard/globalLeaderboard.module.css";
import { useState, useEffect } from "react";
import { collection, getDocs, updateDoc, doc, query, orderBy } from "firebase/firestore";
import { db } from "../services/firebase-config";

function GlobalLeaderboard() {
  const { currentUser } = useSelector((state) => state.user);
  const [users, setUsers] = useState([]);
  const usersCollectionRef = collection(db, "users");
  const q = query(usersCollectionRef, orderBy("score", "desc"));

  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(q);
      setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [q]);

  let rankNow = 0;
  const assignRank = async (id) => {
    const userDoc = doc(db, "users", id);
    const rankFields = { rank: rankNow };
    await updateDoc(userDoc, rankFields);
  };

  if (currentUser) {
    return (
      <div className={styles.main}>
        <Index />
      </div>
    );
  } else {
    return (
      <div className={styles.main}>
        <div className="row">
          <div className="col">
            <h2 className={styles.judul}>GLOBAL LEADERBOARD</h2>
          </div>
        </div>

        <table className={styles.table}>
          <tr className={styles.tr}>
            <th className={styles.th}>Rank</th>
            <th className={styles.th}>Username</th>
            <th className={styles.th}>Total Score</th>
            <th className={styles.th}>Game Suit</th>
            <th className={styles.th}>Dummy1</th>
            <th className={styles.th}>Dummy2</th>
          </tr>
          {users.map((user) => {
            rankNow++;
            assignRank(user.id);
            return (
              <>
                <tr className={styles.tr}>
                  <td className={styles.td}>{user.rank}</td>
                  <Link href={{ pathname: "/seeProfile", query: { data: JSON.stringify(user) } }}>
                    <a>
                      <td className={styles.td}>{user.userName}</td>
                    </a>
                  </Link>
                  <td className={styles.td}>{user.score}</td>
                  <td className={styles.td}>{user.scoreSuit}</td>
                  <td className={styles.td}>{user.scoreDummy1}</td>
                  <td className={styles.td}>{user.scoreDummy2}</td>
                </tr>
              </>
            );
          })}
        </table>
        <br></br>
      </div>
    );
  }
}

export default GlobalLeaderboard;
