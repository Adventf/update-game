import styles from "../styles/Home.module.css";
import Link from "next/dist/client/link";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { db } from "../services/firebase-config";
import { collection, getDocs } from "firebase/firestore";

export default function Home() {
  const [curUser, setCurUser] = useState({});
  const usersCollectionRef = collection(db, "users");
  const { currentUser } = useSelector((state) => state.user);
  useEffect(() => {
    const getUser = async () => {
      const data = await getDocs(usersCollectionRef);
      let allUsers = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
      setCurUser(allUsers.find((x) => x.email == currentUser?.email));
    };
    getUser();
  }, [usersCollectionRef, currentUser]);

  const renderUsername = () => {
    if (currentUser) {
      return (
        <div>
          <Link href="/profile">
            <h2 className={styles.username}>Hello, {curUser?.userName}</h2>
          </Link>
          <div className={styles.grid}>
            <Link href="/home">
              <h1 className={styles.card} href="/home">
                PLAY GAMES
              </h1>
            </Link>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <h2 className={styles.username}>You must log in to play, but feel free to see our games!</h2>
          <div className={styles.grid}>
            <Link href="/Auth/Register">
              <h1 className={styles.card}>REGISTER</h1>
            </Link>
            <Link href="/Auth/Login">
              <h1 className={styles.card} href="/Auth/Login">
                LOGIN
              </h1>
            </Link>
            <Link href="/home">
              <h1 className={styles.card} href="/home">
                SEE GAMES
              </h1>
            </Link>
          </div>
        </div>
      );
    }
  };

  return (
    <div className={styles.main}>
      <a className={styles.title}>WELCOME TO</a>
      <a className={styles.title}>MICROGAMES</a>
      <br></br>
      {renderUsername()}
    </div>
  );
}
