import firebase from "firebase/compat/app";

// import "firebase/database";
import "firebase/compat/auth";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBAKdqaBah0gPCl_QEIiaGkCFRD3zb5VFU",
  authDomain: "challenge9-76726.firebaseapp.com",
  databaseURL: "https://challenge9-76726-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "challenge9-76726",
  storageBucket: "challenge9-76726.appspot.com",
  messagingSenderId: "660220505209",
  appId: "1:660220505209:web:7fef01005b5444c484b8a8",
  measurementId: "G-70WKMNGXZ9",
};

const db = getFirestore(firebase.initializeApp(firebaseConfig));

// const db = firebaseDB.database().ref();
const auth = firebase.auth();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();

export { auth, googleAuthProvider, facebookAuthProvider, githubAuthProvider, db };
